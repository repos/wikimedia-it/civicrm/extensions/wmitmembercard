<?php

require_once 'wmitmembercard.civix.php';
// phpcs:disable
use CRM_Wmitmembercard_ExtensionUtil as E;
// phpcs:enable

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/
 */
function wmitmembercard_civicrm_config(&$config)
{
  _wmitmembercard_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function wmitmembercard_civicrm_install()
{
  _wmitmembercard_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function wmitmembercard_civicrm_enable()
{
  _wmitmembercard_civix_civicrm_enable();
}

function wmitmembercard_civicrm_alterMailParams(&$params, $context)
{
  // TODO: add a setting page
  $template_id = 137;
  if (
    ((isset($_POST['template']) && $_POST['template'] == $template_id) ||
      (isset($params['messageTemplateID']) && $params['messageTemplateID'] == $template_id)
    )
    && $context == 'singleEmail'
    && isset($params['contactId'])
  ) {
    $contactId = $params['contactId'];

    list($fileName, $pdfContent) = CRM_Wmitmembercard_Utils::generateMemberCard($contactId, TRUE);
    $pdf_filename = CRM_Core_Config::singleton()->templateCompileDir . CRM_Utils_File::makeFileName($fileName);
    file_put_contents($pdf_filename, $pdfContent);

    $params['attachments'][] = [
      'fullPath' => $pdf_filename,
      'mime_type' => 'application/pdf',
      'cleanName' => $fileName,
    ];
  }
}

function wmitmembercard_civicrm_summaryActions(&$actions, $contactID)
{
  $actions['otherActions']['membercard'] = [
    'title' => 'Tessera',
    'weight' => 999,
    'ref' => 'member-card',
    'key' => 'membercard',
    'href' => CRM_Utils_System::url('civicrm/membercard', 'cid=' . $contactID),
    'class' => 'no-popup'
  ];
}
