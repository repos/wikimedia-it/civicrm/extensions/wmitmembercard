{literal}
  <style>
    @media print,
    dompdf {

      html,
      body,
      p {
        border: 0;
        font-style: inherit;
        font-weight: inherit;
        margin: 0;
        outline: 0;
        padding: 0;
        vertical-align: baseline;
      }

      html {
        -webkit-text-size-adjust: 100%;
        -ms-text-size-adjust: 100%;
        font-size: 1rem;
        font-family: 'Roboto Slab', serif;
      }

      *,
      *:before,
      *:after {
        box-sizing: border-box;
      }

      main {
        display: block;
        width: 85.00mm;
        height: 55.0mm;
      }

      main img {
        display: block;
        width: 85.00mm;
        height: 55.0mm;
        position: absolute;
        top: 0 left: 0;
        z-index: -1;
      }

      p {
        z-index: 1;
        font-size: 6pt;
        line-height: 1;
        color: black;
        font-weight: bold;
        position: absolute;
        left: 7.4mm;
        white-space: nowrap;
      }

      #nome {
        top: 31.90mm;
      }

      #indirizzo {
        top: 37.57mm;
      }

      #numero {
        top: 42.94mm;
        left: 15.6mm;
        letter-spacing: .1rem;
      }
    }
  </style>
{/literal}
<main>
  <img src="{$backgroundimgfront}">
  <p id="nome">{$display_name}</p>
  <p id="indirizzo">{$street_address} - {$postal_code} {$city} {$state_province}</p>
  <p id="numero">{$membership_number}</p>
</main>