# wmitmembercard

![Screenshot](/images/screenshot.png)

(*FIXME: In one or two paragraphs, describe what the extension does and why one would download it. *)

The extension is licensed under [AGPL-3.0](LICENSE.txt).

## Requirements

* PHP v7.2+
* CiviCRM (*FIXME: Version number*)

## Installation (Web UI)

Learn more about installing CiviCRM extensions in the [CiviCRM Sysadmin Guide](https://docs.civicrm.org/sysadmin/en/latest/customize/extensions/).

## Installation (CLI, Zip)

Sysadmins and developers may download the `.zip` file for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
cd <extension-dir>
cv dl wmitmembercard@https://github.com/FIXME/wmitmembercard/archive/master.zip
```

## Installation (CLI, Git)

Sysadmins and developers may clone the [Git](https://en.wikipedia.org/wiki/Git) repo for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
git clone https://github.com/FIXME/wmitmembercard.git
cv en wmitmembercard
```

## Getting Started

Yet to be automated :
* create a custom pager size -> civicrm/admin/options?reset=1 -> paper_size
* create a custom pdf format for the membership card -> civicrm/admin/pdfFormats
* create a mail template for sending the membership card
* change ID for PDF format and mail template in the code (needs settings for those)


## Known Issues

(* FIXME *)
