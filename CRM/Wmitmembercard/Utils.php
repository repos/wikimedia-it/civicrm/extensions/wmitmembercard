<?php

class CRM_Wmitmembercard_Utils {

  public static function generateMemberCard($contactId, $output=FALSE) {

    // FIXME: might not be 100% reliable if the site contains several type of membership
    // we should add a setting to limit the membership types that gives access to a card
    $result = civicrm_api3('Membership', 'get', [
      'sequential' => 1,
      'return' => ['contact_id.display_name', 'custom_23'],
      'contact_id' => $contactId,
      'options' => ['sort' => 'end_date desc'],
      'active_only' => 1,
      'is_test' => 0,
    ]);

    if ($result['count'] > 0) {

      $res = $result['values'][0];

      $contacts = civicrm_api3('Contact', 'get', [
        'sequential' => 1,
        'return' => ['street_address', 'city', 'state_province', 'postal_code'],
        'id' => $contactId,
      ]);

      if ($contacts['count'] > 0) {

        $res = array_merge($res, $contacts['values'][0], ['membership_number' => $res['custom_23']]);

        // assign info to template
        $smarty = CRM_Core_Smarty::singleton();
        foreach ($res as $name => $value) {
          $name = str_replace('contact_id.', '', $name);
          $smarty->assign($name, $value);
        }

        // TODO: make it a setting
        $smarty->assign('backgroundimgfront', 'https://sostieni.wikimedia.it/sites/default/files/tessera_sociale.jpg');

        // call template
        $content = $smarty->fetch('CRM/Wmitmembercard/MembershipCard.tpl');

        // TODO: make it a setting (multilingual)
        $fileName = 'Tessera.pdf';

        // 'Membership Card' page format
        // TODO: make it a setting
        $pageFormat = 1235;

        $pdf = CRM_Utils_PDF_Utils::html2pdf($content, $fileName, $output, $pageFormat);
        if ($output) {
          return [$fileName, $pdf];
        }
        CRM_Utils_System::civiExit();
      }
    }
  }
}
