<?php
use CRM_Wmitmembercard_ExtensionUtil as E;

class CRM_Wmitmembercard_Page_Membercard extends CRM_Core_Page {

  public $_contactId = NULL;

  public function run() {
    $this->_contactId = CRM_Utils_Request::retrieve('cid', 'Positive', $this);

    if (CRM_Core_Permission::check('administer CiviCRM')) {
      if (empty($this->_contactId)) {
        // force current logged in contact for non-admin
        $userId = CRM_Core_Session::getLoggedInContactID();
        $this->_contactId = $userId;
      }
    }
    else {

      // check logged in user permission
      if (!CRM_Core_Permission::check('CiviCRM: access Contact Dashboard')) {
        CRM_Core_Error::statusBounce(ts('You are not authorized to access this page.'));
        return;
      }

      // force current logged in contact for non-admin
      $userId = CRM_Core_Session::getLoggedInContactID();
      $this->_contactId = $userId;

      $this->assign('contactId', $this->_contactId);

    }

    CRM_Wmitmembercard_Utils::generateMemberCard($this->_contactId);
  }
}
